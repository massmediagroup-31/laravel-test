@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(array('route' => ['post.update', $post->id, $tagsOfPost->pluck('id')], 'method' => 'PATCH')) }}
                @if (count($errors))
                    @component('layouts.components.alert')
                    @endcomponent
                @endif

                @if (session('error'))
                    @component('layouts.components.error')
                    @endcomponent
                @endif

                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif

                @if (session('success-message-arr'))
                    @component('layouts.components.success-message-arr')
                    @endcomponent
                @endif

                @if (session('warning-message-arr'))
                    @component('layouts.components.warning-message-arr')
                    @endcomponent
                @endif

                <div class="card text-center">
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'title', 'required' => 'required', 'value' => $post->title])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        @component('layouts.components.textarea',
                        ['title' => 'description', 'required' => 'required', 'value' => $post->description])
                        @endcomponent
                    </div>
                    <div class="card-body">
                        <tag-editor
                                :tags-of-post = "{{ $tagsOfPost }}"
                                :all-tags = "{{ $allTags }}"
                        >
                        </tag-editor>
                    </div>
                </div>
                <br>
                {{ Form::submit('Edit',  ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}

                {{ Form::open(array('route' => ['post.delete', $post->id], 'method' => 'DELETE')) }}
                {{ Form::submit('Delete Post',  ['class' => 'btn btn-danger float-right']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
