@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif
                @foreach($posts as $post)
                    <div class="card text-center">
                        <div class="card-header">
                            <h3> <a href="{{ route('post.edit', [$post->id]) }}"> {{ $post->title }} </a> </h3>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">
                                @foreach($post->tags as $tag)
                                    {{ $loop->first ? '' : ', ' }}
                                    {{ $tag->title }}
                                @endforeach
                            </h5>
                            <p class="card-text">{{ $post->description }}</p>
                        </div>
                    </div>
                    <br>
                @endforeach
                {{ $posts->links() }}
            </div>
        </div>
    </div>
@endsection