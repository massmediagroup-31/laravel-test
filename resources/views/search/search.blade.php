@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <h2 class="text-center">Search result by {{ $searchBy }}</h2>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (count($foundPosts))
                    @foreach($foundPosts as $post)
                        <div class="card text-center">
                            <div class="card-header">
                                <h3> {{ $post->title }} </h3>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">
                                    @foreach($post->tags as $tag)
                                        {{ $loop->first ? '' : ', ' }}
                                        {{ $tag->title }}
                                    @endforeach
                                </h5>
                                <p class="card-text">{{ $post->description }}</p>
                            </div>
                        </div>
                        <br>
                    @endforeach
                @else
                    <h3 class="text-center">Nothing found</h3>
                @endif
            </div>
        </div>
    </div>
@endsection