@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="text-center mb-2">
            <a class="btn btn-primary" href="{{ route('tag.create') }}">Create tag</a>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($tags as $tag)
                    <div class="card text-center">
                        <div class="card-header">
                            <h3>
                                <a href="{{ route('tag.edit', [$tag->id]) }}"> {{ $tag->title }} </a>
                                {{ Form::open(array('route' => ['tag.delete', $tag->id], 'method' => 'DELETE')) }}
                                {{ Form::submit('&times;',  ['class' => 'btn btn-primary']) }}
                                {{ Form::close() }}
                            </h3>
                        </div>
                    </div>
                    <br>
                @endforeach
                {{ $tags->links() }}
            </div>
        </div>
    </div>
@endsection