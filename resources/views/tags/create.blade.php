@extends('layouts.app')

@section('content')
    <div class="container" id="app">
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{ Form::open(array('route' => 'tag.store', 'method' => 'POST')) }}
                @if (count($errors))
                    @component('layouts.components.alert')
                    @endcomponent
                @endif

                @if (session('success'))
                    @component('layouts.components.success')
                    @endcomponent
                @endif

                <div class="card text-center">
                    <div class="card-header">
                        @component('layouts.components.input',
                        ['title' => 'title', 'required' => 'required'])
                        @endcomponent
                    </div>
                </div>
                <br>
                {{ Form::submit('Create',  ['class' => 'btn btn-primary']) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
@endsection
