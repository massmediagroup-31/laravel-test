<div class="alert alert-warning">
    <ul>
        <span> Deleted: </span>
        @foreach(session('warning-message-arr') as $success)
            <li>{{ $success }}</li>
        @endforeach
    </ul>
</div>