{{ Form::textarea($title, $value ?? null,
    ['placeholder' => ucfirst($title), 'class' => 'form-control', 'required' => $required]
)}}