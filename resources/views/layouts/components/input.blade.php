{{ Form::text($title, $value ?? null,
    [
    'placeholder' => $placeholder ?? ucfirst($title),
    'value' => $value ?? null,
    'class' => 'form-control',
    'required' => $required,
    ]
)}}