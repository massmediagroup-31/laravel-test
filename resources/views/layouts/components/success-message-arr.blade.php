<div class="alert alert-success">
    <ul>
        <span> Added: </span>
        @foreach(session('success-message-arr') as $success)
            <li>{{ $success }}</li>
        @endforeach
    </ul>
</div>