<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    private $post;
    private $tag;

    /**
     * SearchController constructor.
     * @param Post $post
     * @param Tag $tag
     */
    public function __construct(Post $post, Tag $tag)
    {
        $this->post = $post;
        $this->tag = $tag;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $keyWords = $request->get('q');
        $posts = $this->post->searchPost($keyWords);
        $searchBy = 'post';

        if (count($posts) != null) {
            return view('search.search', compact('posts', 'searchBy'));
        }
        $searchBy = 'tag';
        // if post title not found, find by tag

        $foundPosts = $this->post->searchPostByTag($keyWords);

        return view('search.search', compact('foundPosts', 'searchBy'));
    }
}
