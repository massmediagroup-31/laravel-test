<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostStoreRequest;
use App\Post;
use App\Tag;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\PostUpdateRequest;

class PostController extends Controller
{
    private $post;
    private $tag;

    public function __construct(Post $post, Tag $tag)
    {
        $this->post = $post;
        $this->tag = $tag;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = $this->post->orderBy('created_at', 'desc')->paginate(10);

        return view('posts.index', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $allTags = $this->tag->all();

        return view('posts.create', compact('allTags'));
    }

    /**
     * @param PostStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostStoreRequest $request)
    {
        $data = $request->all();

        $post = $this->post->create($data);

        // save tags in DB
        if (isset($data['tags_ids']) != null) {
            $post->tags()->attach($data['tags_ids']);
        }

        $request->session()->flash('success', 'Post successfully added!');

        return redirect()->route('post.edit', compact('post'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $id)
    {
        $post = $this->post->find($id);
        $tagsOfPost = $post->tags;

        // take all tags
        $allTags = $this->tag->all();

        return view('posts.edit', compact('post', 'tagsOfPost', 'tagsNotPost', 'allTags'));
    }

    public function update(PostUpdateRequest $request, $id)
    {
        $data = $request->all();
        $post = $this->post->find($id);

        $idsTagsOfPost = $post->tags->pluck('id')->toArray();

        // check delete all tags or not all
        if ( !isset($data['tags_ids']) ) {
            $tagsForDelete = $idsTagsOfPost;
        } else {
            $tagsForDelete = array_diff($idsTagsOfPost, $data['tags_ids']);
        }

        $titlesDeletedTags = $this->tag->deleteTags($tagsForDelete, $post);

        if ($titlesDeletedTags) {
            $request->session()->flash('warning-message-arr', $titlesDeletedTags);
        }

        // update tags
        if (isset($data['tags_ids']) != null) {

            $resultUpdate = $this->tag->updateTags($data['tags_titles'], $data['tags_ids']);

            if (!$resultUpdate) {
                $request->session()->flash('error', 'Empty tag');

                return Redirect::back();
            }
        }
        // $newTags - tags we added to posts
        if ( isset($data['tags_ids']) ) {
            $newTags = array_diff($data['tags_ids'], $idsTagsOfPost);
            if ( $newTags ) {
                $post->tags()->attach($newTags);
                $addedTags = $this->tag->find($newTags)->pluck('title')->toArray();
                $request->session()->flash('success-message-arr', $addedTags);
            }
        }

        $post->update($data);

        return Redirect::back()->with('success', 'Operation Successful!');
    }


    public function delete(int $id)
    {
        $this->post->find($id)->delete();

        session()->flash('success', 'Post deleted');
        return redirect()->route('post.index');
    }
}
