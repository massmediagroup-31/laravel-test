<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagCreateRequest;
use App\Http\Requests\TagUpdateRequest;
use App\Tag;
use Illuminate\Support\Facades\Redirect;

class TagController extends Controller
{

    private $tag;

    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function index()
    {
        $tags = $this->tag->orderBy('created_at', 'desc')->paginate(10);;
        return view('tags.index', compact('tags'));
    }

    public function create()
    {
        return view('tags.create');
    }

    public function store(TagCreateRequest $request)
    {
        $tag = $this->tag->create($request->all());

        $request->session()->flash('success', 'Tag successfully added');

        return redirect()->route('tag.edit', [$tag->id]);

    }

    public function edit(int $id)
    {
        $tag = $this->tag->find($id);

        return view('tags.edit', compact('tag'));
    }

    public function update(TagUpdateRequest $request, $id)
    {
        $data = $request->all();
        $tag = $this->tag->find($id);

        $tag->update($data);

        return Redirect::back()->with('success', 'Tag edited');
    }

    public function delete($id)
    {
        $tag = $this->tag->find($id);

        $tag->delete();

        return Redirect::back()->with('success', 'Tag deleted');
    }
}
