<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{

    protected $ips = [
        '127.0.0.',
        '188.102.29.159',
    ];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (in_array($request->ip(), $this->ips)) {
            return redirect('/access-denied');
        }

        return $next($request);
    }
}
