<?php

namespace App\Exceptions;

class CreatePostErrorException extends \Exception
{
    public function __construct()
    {
        echo 'Error create post';
    }
}