<?php

namespace App\Exceptions;

class UpdatePostErrorException extends \Exception
{
    public function __construct()
    {
        echo 'Error update post';
    }
}