<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Post extends Model
{

    protected $fillable = ['title', 'description'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function post_images()
    {
        return $this->hasMany('App\PostImage');
    }

    public function searchPost($keyWords)
    {
        return Post::where('title','LIKE','%'.$keyWords.'%')
            ->get();
    }

    /**
     * @param $keyWords
     * @return mixed
     */
    public function searchPostByTag($keyWords)
    {
        $posts = Post::whereHas('tags', function (Builder $query) use ($keyWords){
            $query->where('title', 'like', "$keyWords".'%');
        })->get();

        return $posts;
    }

}
