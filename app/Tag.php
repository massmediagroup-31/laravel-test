<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }

    /**
     * @param array $tagsTitles
     * @param array $tagsIds
     * @return bool
     */
    public function updateTags(array $tagsTitles, array $tagsIds)
    {
        for ($i = 0; $i < count($tagsIds); $i++) {

            if ( !$tagsTitles[$i] ) {
                return false;
            }

            Tag::find($tagsIds[$i])->update(['title' => $tagsTitles[$i]]);
        }

        return true;
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function deleteTags($ids, $post)
    {
        $post->tags()->detach($ids);

        $deletedTagsTitles = Tag::find($ids)->pluck('title')->toArray();

        return $deletedTagsTitles;
    }
}
