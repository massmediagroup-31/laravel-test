<?php

namespace App\Repositories;

use App\Post;
use App\Exceptions\CreatePostErrorException;
use App\Exceptions\UpdatePostErrorException;
use App\Exceptions\PostNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class PostRepository
{
    /**
     * CarouselRepository constructor.
     * @param Post $post
     */
    public function __construct(Post $post)
    {
        $this->model = $post;
    }

    /**
     * @param array $data
     * @return Post
     * @throws CreatePostErrorException
     */
    public function createPost(array $data): Post
    {
        try {
            return $this->model->create($data);
        } catch (QueryException $e) {
            throw new CreatePostErrorException($e);
        }
    }

    /**
     * @param array $data
     * @return Post
     * @throws UpdatePostErrorException
     */
    public function updatePost(array $data): bool
    {
        try {
            return $this->model->update($data);
        } catch (QueryException $e) {
            throw new UpdatePostErrorException($e);
        }
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function deletePost(): bool
    {
        return $this->model->delete();
    }

    /**
     * @param int $id
     * @return Post
     * @throws PostNotFoundException
     */
    public function findPost(int $id) : Post
    {
        try {
            return $this->model->findOrFail($id);
        } catch (ModelNotFoundException $e) {
            throw new PostNotFoundException($e);
        }
    }
}