<?php

namespace Tests\Unit\Posts;

use Tests\TestCase;
use App\Post;
use App\Repositories\PostRepository;
use App\Exceptions\CreatePostErrorException;
use App\Exceptions\PostNotFoundException;

class PostUnitTest extends TestCase
{
    /** @test */
    public function it_can_create_a_post()
    {
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->word,
        ];

        $postRepo = new PostRepository(new Post);
        $post = $postRepo->createPost($data);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertEquals($data['title'], $post->title);
        $this->assertEquals($data['description'], $post->description);
    }

    /** @test */
    public function it_can_update_a_post()
    {
        $post = factory(Post::class)->create();

        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->word,
        ];

        $postRepo = new PostRepository($post);
        $update = $postRepo->updatePost($data);

        $this->assertTrue($update);
        $this->assertEquals($data['title'], $post->title);
        $this->assertEquals($data['description'], $post->description);
    }

    /** @test */
    public function it_can_delete_a_post()
    {
        $post = factory(Post::class)->create();

        $postRepo = new PostRepository($post);
        $delete = $postRepo->deletePost();

        $this->assertTrue($delete);
    }

    // negative tests

    /** @test */
    public function it_should_throw_an_error_when_the_required_columns_are_not_filled()
    {
        $this->expectException(CreatePostErrorException::class);

        $postRepo = new PostRepository(new Post);
        $postRepo->createPost(['title'=>null]);
    }

    /** @test */
    public function it_should_throw_not_found_error_exception_when_the_post_is_not_found()
    {
        $this->expectException(PostNotFoundException::class);

        $postRepo = new PostRepository(new Post);

        $postRepo->findPost(999);

    }
}