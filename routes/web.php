<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/access-denied', function () {
    return view('access-denied');
});

Route::get('/search', 'SearchController@search')->name('search');

Route::middleware(['restrictIp'])->group(function () {

    Route::get('/', 'PostController@index')->name('post.index');

    Route::get('/posts/create', 'PostController@create')->name('post.create');
    Route::post('/posts/store', 'PostController@store')->name('post.store');
    Route::get('/posts/edit/{id}', 'PostController@edit')->name('post.edit');
    Route::patch('/posts/update/{post}', 'PostController@update')->name('post.update');
    Route::delete('/posts/delete/{id}', 'PostController@delete')->name('post.delete');

    Route::get('/tags', 'TagController@index')->name('tag.index');
    Route::get('/tags/create', 'TagController@create')->name('tag.create');
    Route::post('/tags/store', 'TagController@store')->name('tag.store');
    Route::get('/tags/edit/{id}', 'TagController@edit')->name('tag.edit');
    Route::patch('/tags/update/{tag}', 'TagController@update')->name('tag.update');
    Route::delete('/tags/delete/{id}', 'TagController@delete')->name('tag.delete');
});